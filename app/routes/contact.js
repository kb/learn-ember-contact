import Ember from 'ember';

let contacts = [{
  id: 'grand-old-mansion',
  name: 'Pynto',
  website: 'http://www.pynto.co.uk/',
  postcode: 'EX33 3CC',
}, {
  id: 'urban-living',
  name: 'Tally Ho',
  website: 'http://tallyhohatherleigh.co.uk/',
  postcode: 'EX22 2BB',
}, {
  id: 'downtown-charm',
  name: 'Tidball',
  website: 'http://www.tidball.me.uk/',
  postcode: 'EX11 1AA',
}];

export default Ember.Route.extend({
  model() {
    return contacts;
  }
});
